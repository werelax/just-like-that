(defproject jlt "0.1.0"
  :description "The next best thing"
  :url "http://justlikethat.io"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 ;; cljs
                 [org.clojure/clojurescript "0.0-2850"]
                 ;; the super cool core.async
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 ;; ring
                 [ring/ring-core "1.3.2"]
                 [ring/ring-defaults "0.1.3"]
                 [ring/ring-devel "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [fogus/ring-edn "0.2.0"]
                 ;; crypto (hashing)
                 [pandect "0.5.0"]
                 ;; web stack
                 [compojure "1.3.1"]
                 [enlive "1.1.5"]
                 [http-kit "2.1.18"]
                 ;; json (not needed because of ring-json)
                 ;; [org.clojure/data.json "0.2.5"]
                 ;; bbdd
                 [org.clojure/java.jdbc "0.3.6"]
                 ;; [org.xerial/sqlite-jdbc "3.8.7"]
                 [com.h2database/h2 "1.3.176"]
                 [lobos "1.0.0-beta3"]
                 ;; (korma needs jdbc 0.3.5 specifically)
                 [org.clojure/java.jdbc "0.3.5"]
                 [korma "0.4.0"]
                 ;; json
                 [org.clojure/data.json "0.2.5"]
                 ;; cljs libraries
                 [secretary "1.2.2-SNAPSHOT"]
                 [cljsjs/react "0.12.2-7"]
                 [om "0.8.0-rc1"]
                 [kioo "0.4.1-SNAPSHOT"]
                 [jayq "2.5.4"]
                 ]
  :plugins [[lein-cljsbuild "1.0.6-SNAPSHOT"]
            [lein-pdo "0.1.1"]
            ;; (NOTE: lein-ring doesn't support http-kit yet)
            ;; [lein-ring "0.9.1"]
            ]

  ;; :ring {:handler ljt.core}

  :cljsbuild {
              :builds [{:id "dev"
                        :source-paths ["cljs-src"]
                        :compiler {
                                   :output-to "resources/public/assets/js/main.js"
                                   :optimizations :whitespace
                                   :pretty-print true
                                   :source-maps true
                                   :preamble ["react/react.min.js"]
                                   }}]}
  :aliases {"up" ["pdo" "cljsbuild" "auto" "dev," "run"]}

  :aot [jlt.core]

  :jvm-opts ^:replace ["-Xmx256m" "-server"]

  :main jlt.core)

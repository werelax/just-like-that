
$(document).ready(function() {
  $('.main-header .menu-icon').click(function() {
    var windowWidth = $(window).width();
    if(windowWidth < 890) {
      $('.main-nav').toggleClass('open-nav');
    }
  });

  $('.sample .main-list li.tab').click(function(e) {
    e.preventDefault();
    var target = $(this).attr("data-target");
    $('.sample-img').removeClass('solid');
    $(target).addClass('solid');
    $('.sample .main-list li').removeClass('active');
    $(this).addClass('active');
  });
});


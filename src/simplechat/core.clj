(ns simplechat.core
  (:require [org.httpkit.server :as hk :refer [with-channel open? send!]]
            [clojure.set :as set]))

(def rooms (atom {}))
;; {23 {:users #{ {:channel #<AsyncChannel> :username Jarl} }, :log ["asdf"]}}

(def id (atom 0))

(defn- emtpy-room []
  {:users #{} :log []})

(defn close-room [room-number]
  (swap! rooms dissoc room-number))

(defn- me-or-closed [my-channel user]
  (let [channel (:channel user)]
    (or (= channel my-channel)
        (not (open? channel)))))

(defn- unbind-channel [room-number channel status]
  (let [room (get @rooms room-number)
        users (:users room)
        to-remove (filter (partial me-or-closed channel)
                          users)
        left (set/difference users (set to-remove))]
    ;; NOTE: not sure if deleting the room is a good idea?
    ;; NOTE: not keeping it in memory, but persist it to de DB!
    (if (= 0 (count left))
      (close-room room-number)
      (swap! rooms assoc-in [room-number :users] left))))

(defn- broadcast-to-room [room-number channel data]
  (let [room (get @rooms room-number)
        users (:users room)
        targets (remove (partial me-or-closed channel)
                        users)]
    (doseq [recv targets]
      (send! (:channel recv) data))))

(defn- process-message [room-number channel data]
  ;; I think this is enough! let the client deal with the data?
  (let [log (:log @rooms)]
    (swap! rooms assoc :log (conj log data)))
  (broadcast-to-room room-number channel data))

(defn add-user-to-room [room-number user channel]
  (let [user-data (assoc user :channel channel)
        room (get @rooms room-number (emtpy-room))
        users (:users room)]
    (swap! rooms assoc room-number (assoc room :users
                                          (conj users user-data)))))

(defn bind-channel-to-room [room user channel]
  (add-user-to-room room user channel)
  (hk/on-close channel (partial unbind-channel room channel))
  (hk/on-receive channel (partial process-message room channel)))

(defn handler [req room user]
  (with-channel req channel
    (bind-channel-to-room room user channel)))

(defn create-room []
  (swap! id inc)
  (swap! rooms assoc @id (emtpy-room))
  ;; create the room vector
  @id)

(defn report-status []
  false)

(ns simpleauth.core
  (:require [ring.util.response :refer [redirect response status]]
            [compojure.core :as compojure]))

(defprotocol AuthStrategy
  "Defines a strategy to authenticate users"
  (serialize-user [_ user] "user map -> user id")
  (deserialize-user [_ user-id] "user id -> user map")
  (check-credentials? [_ username password] "pred: are the credentials correct?")
  (login-route [_] "returns: route to redirect when unauthorized"))

(defn- get-session [res req]
  (:session res (:session req {})))

(defn- get-id [user]
  (:id user (get user "id")))

(defn- assoc-in-session [response session user-id]
  (assoc response :session
         (assoc session :user-id user-id)))

(defn- logout-and-redirect [strategy session options]
  (-> (redirect (or (:fail options)
                    (login-route strategy)))
    (assoc :session (dissoc session :user-id))))

(defn- logout [res req]
  (let [session (get-session res req)]
    (assoc res :session (dissoc session :user-id))))

(defn create-session [req strategy {user :username pass :password :as options}
                      handler err-handler]
  (if-let [user (check-credentials? strategy user pass)]
    (let [res (handler req user)
          session (get-session res req)]
      (assoc res :session (assoc session :user-id
                                 (serialize-user strategy user))))
    (logout (err-handler req) req)))

(defn destroy-session [req res]
  (let [session (get-session res req)]
    (assoc res :session
           (dissoc session :user-id))))

(defn login [{:keys [req res user]}]
  (assoc-in-session res (get-session res req) (get-id user)))

(defn- is-logged? [strategy session]
  (when (contains? session :user-id)
    (deserialize-user strategy (:user-id session))))

(defn with-auth [strategy & routes]
  (let [handler (apply compojure/routes routes)]
    (fn [{session :session :as req}]
      (if-let [user (is-logged? strategy session)]
        (handler (assoc req :session
                        (assoc session :user user)))
        (logout-and-redirect strategy session {})))))

(defn with-api-auth [strategy & routes]
  (let [handler (apply compojure/routes routes)]
    (fn [{session :session :as req}]
      (if-let [user (is-logged? strategy session)]
        (handler (assoc req :session
                        (assoc session :user user)))
        (status (response "") 401)))))

(ns jlt.models.post
  (:refer-clojure :exclude [find])
  (:require [korma.core :as db]
            [jlt.entities :refer [posts users] :as entities]
            [pandect.algo.md5 :refer :all]))

(def ^:private per-page 100)

(defn find-all [page]
  (db/select posts
             (db/limit per-page)
             (db/offset (* page per-page))
             (db/with users
                      (db/fields [:username :user.name]
                                 [:gravatar :user.gravatar]))))

(defn find-for-user [user-id]
  (db/select posts
             (db/where {:users_id user-id})
             (db/limit 10)
             (db/order :created_at :DESC)))

(defn create [data]
  (let [last-id (:id (db/insert posts (db/values data)))]
    (assoc data :id last-id)))

(defn create-with-user [data user]
  (let [data (assoc data
                    :users_id (:id user)
                    :created_at (.getTime (java.util.Date.)))]
    (create data)))

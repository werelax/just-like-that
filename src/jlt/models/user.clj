(ns jlt.models.user
  (:refer-clojure :exclude [find])
  (:require [korma.core :as db]
            [jlt.entities :refer [all-users posts id] :as entities]
            [pandect.algo.md5 :refer :all]))

(def ^:private posts-per-page 100)

(defn create [json]
  "create a new user in the database"
  (let [gravatar-url (str "http://www.gravatar.com/avatar/" (md5 (get json :email "")))
        user-json (assoc json :gravatar gravatar-url)
        last-id (id (db/insert all-users (db/values user-json)))]
    (assoc user-json :id last-id)))

(defn find [user-id page]
  (db/select all-users
             (db/where {:id user-id})
             (db/with posts
                      (db/limit posts-per-page)
                      (db/offset (* page posts-per-page)))))

;; Count messages
;; TODO: this is SO wrong in SO MANY levels....
(defn get-user-info [user-id]
  (first (db/select all-users
                    (db/where {:id user-id})
                    (db/with posts
                             (db/order :created_at :DESC)))))

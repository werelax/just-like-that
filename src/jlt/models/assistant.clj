(ns jlt.models.assistant
  (:refer-clojure :exclude [find])
  (:require [korma.core :as db]
            [jlt.entities :refer [users id] :as entities]
            [jlt.models.user :as user-model]))

(defn create [json]
  (user-model/create (assoc json :assistant 1)))

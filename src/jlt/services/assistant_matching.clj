(ns jlt.services.assistant-matching
  (:require [jlt.services.online-presence :as presence]
            [jlt.services.chat :as chat]))

(def ^:dynamic *notification-timeout* (* 10 1000))

(defn get-all-available []
  (presence/get-present-users
   #(and (= (get-in % [:user :assistant]) 1)
         (= (:activity %) :free))))

(defn mark-busy [assistant]
  (presence/set-activity assistant :busy))

(defn mark-available [assistant]
  (presence/set-activity assistant :free))

(defn confirm-query [user assistant]
  ;; TODO: II.  double check that the user is still online
  ;; TODO: III. rembember to clear the activity state!
  ;; TODO: IV.  if the user STAYS CONNECTED BUT DOESN'T SHOW IN THE CHAT ROOM!
  ;;            (we should use two states(pre-chat, in-chat) + a daemon to check)
  (chat/close-rooms-with [user assistant])
  (let [room (chat/create-room user assistant)]
    (doseq [you [user assistant]]
      (doto you
        (presence/set-activity :in-mission)
        (presence/set-chat-room room)
        (chat/add-listeners room)
        (presence/send-message {:command :chat-ready :payload room})))))

(defn abort-query [user]
  (presence/send-message user {:command :query-aborted}))

;; round robin notification code

(defn send-task-notification [assistant type p]
  (println "sending message for" type "to" assistant)
  (presence/add-once-listener assistant :answer-to-request #(deliver p %))
  (presence/send-message assistant {:command :task-request :payload type}))

(defn ask-assistant [{assistant :user} type]
  (mark-busy assistant)
  (let [p (promise)]
    (send-task-notification assistant type p)
    (let [response (deref p *notification-timeout* :timeout)]
      (mark-available assistant)
      response)))

(defn notify-next-assistant [{:keys [type queue user] :as data}]
  ;; TODO: should check if the user is still online
  ;; TODO: before continue asking
  (let [next (first queue)]
    (cond
      (empty? queue) (abort-query user)
      (= :ok (ask-assistant next type)) (confirm-query user (:user next))
      :else (notify-next-assistant (assoc data :queue (rest queue))))))

(defn start-request-round [user type]
  (let [queue (get-all-available)
        agent-queue (agent {:user user :type type :queue queue})]
    (send-off agent-queue notify-next-assistant)))

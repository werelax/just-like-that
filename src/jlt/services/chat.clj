(ns jlt.services.chat
  (:require [jlt.services.online-presence :as presence]
            [clojure.pprint :refer [pprint]]))

(defonce ^:dynamic *rooms* (atom {}))

(declare remove-listeners)

(defn- now []
  (System/currentTimeMillis))

(defn- new-room-number []
  "The string concatenation makes collisions very very unlikely"
  (str (rand-int 10000) (now)))

(defn- room-data [number user assistant]
  {:number number :user user :assistant assistant :people #{user assistant}
   :messages [] :results [] :created-at (now)})

(defn- other-user [user room-number]
  (let [room (@*rooms* room-number)]
    (first (disj (:people room) user))))

(defn- send-to-other [user room-number command]
  (let [to (other-user user room-number)]
    (presence/send-message to command)))

(defn- sanitize-user [user]
  (select-keys user [:username :gravatar]))

(defn dismantle-room
  ([room-number]
   (dismantle-room room-number (get-in @*rooms* [room-number :people])))
  ([room-number notify-to]
   ;; TODO: persist room as finished!
   (let [room (@*rooms* room-number)]
     (doseq [user (:people room)]
       (doto user
         (presence/set-activity :free)
         (remove-listeners)))
     (doseq [user notify-to]
       (presence/send-message user {:command :room-closed
                                    :payload room-number}))
     (swap! *rooms* dissoc room-number))))

(defn close-rooms-with [user-list]
  (doseq [user user-list]
    (let [to-close (filter #(let [[n room] %] (contains? (:people room) user))
                           @*rooms*)]
      (doseq [[number room] to-close]
        ;; notify only to the OTHER user in the room
        (dismantle-room number (disj (:people room) user))))))

(defn create-room [user assistant]
  (let [number (new-room-number)]
    (close-rooms-with user)
    (close-rooms-with assistant)
    (swap! *rooms* assoc number (room-data number user assistant))
    number))

(defn room-exists? [number]
  (contains? @*rooms* number))

(defn room-accesible? [number user]
  (and (room-exists? number)
       (let [room (@*rooms* number)]
         (contains? (:people room) user))))

(defn on-my-info [user room-number _]
  (presence/send-message user {:command :report-my-info
                               :payload (sanitize-user user)}))

(defn on-his-info [user room-number _]
  (let [other (sanitize-user (other-user user room-number))]
    (presence/send-message user room-number {:command :report-his-info
                                             :payload other})))

(defn on-send-message [user room-number message]
  (swap! *rooms* update-in [room-number :messages] #(conj % message))
  (send-to-other user room-number {:command :new-message :payload message}))

(defn on-send-result [user room-number result]
  (swap! *rooms* update-in [room-number :results] #(conj % result))
  (send-to-other user room-number {:command :new-result :payload result}))

(defn on-chat-status [to room-number _]
  (let [{:keys [user assistant] :as room} (@*rooms* room-number)
        info (select-keys room [:messages :results :created-at])
        payload (merge info {:user (sanitize-user user)
                             :assistant (sanitize-user assistant)})]
    (presence/send-message to {:command :report-chat-status :payload payload})))

(defn on-finish-task [user room-number _]
  (let [room (@*rooms* room-number)]
    (doseq [you (:people room)]
      (presence/send-message you {:command :task-finished :payload (now)}))
    (dismantle-room room-number)))

(defn on-disconnect [user room-number _]
  "Send the message to both, but only one will get it: the not-disconnected one"
  (let [room (@*rooms* room-number)]
    (doseq [you (:people room)]
      (presence/send-message you {:command :other-disconnected :payload (now)}))
    (dismantle-room room-number)))

(defn in-chat? [user]
  (some #(contains? (:people %) user) @*rooms*))

(def chat-commands [:my-info :his-info :send-message
                    :send-result :finish-task :chat-status
                    :disconnect])

(defn add-listeners [user room-number]
  (doseq [command chat-commands]
    (let [handler (ns-resolve 'jlt.services.chat
                              (symbol (str "on-" (name command))))]
      (presence/add-listener user command (partial handler user room-number)))))

(defn remove-listeners [user]
  (doseq [command chat-commands]
    (presence/remove-listeners user command)))

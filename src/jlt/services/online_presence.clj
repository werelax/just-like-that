(ns jlt.services.online-presence
  (:require [org.httpkit.server :as hk :refer [send!]]))

(declare add-builtin-listeners remove-builtin-listeners)

(def ^:dynamic *connected-users* (atom {}))

(defn- now []
  (System/currentTimeMillis))

(defn- connected-user [user channel]
  {:user user
   :state :online
   :activity :free
   :room nil
   :channel channel
   :last-connection (now)
   :went-offline 0
   :listeners {} })

(defmacro when-online [user & body]
  `(when (get @*connected-users* (:id ~user) nil)
     ~@body))

(defn online [user channel]
  ;; I want to preserve the original data if available
  ;; (cause the user may be just switching pages or reloading)
  (println "user online!" (:username user) "::" (:id user))
  (let [user-info (get @*connected-users* (:id user)
                       (connected-user user channel))]
    (swap! *connected-users* assoc (:id user)
           (assoc user-info :state :online :channel channel ))
    (add-builtin-listeners user)))

(defn offline [user channel _]
  (println "user offline..." (:username user) "::" (:id user))
  (when-online user
   (let [user-info (get @*connected-users* (:id user))]
     (swap! *connected-users* assoc (:id user)
            (assoc user-info :state :offline :went-offline (now))))
   (remove-builtin-listeners user)))

(defn set-activity [user activity]
  (when-online user (swap! *connected-users*
                           assoc-in [(:id user) :activity] activity)))

(defn set-chat-room [user room]
  ;; TODO: FIXME: should I protect against chat-room overwrite?
  (when-online user (swap! *connected-users*
                           assoc-in [(:id user) :room] room)))

(defn get-present-users [pred]
  (filter pred (vals @*connected-users*)))

(defn send-message [user msg-data]
  (when-let [data (get @*connected-users* (:id user))]
    (send! (:channel data) (pr-str msg-data))))

(defn add-listener [user command listener]
  (when-online user
   (swap! *connected-users* update-in [(:id user) :listeners command]
           #(conj % listener))))

(defn remove-listeners [user command]
  (when-online user
    (swap! *connected-users* update-in [(:id user) :listeners]
           #(dissoc % command))))

(defn add-once-listener [user command listener]
  (add-listener user command #(do (remove-listeners user command)
                                  (listener %))))

(defn dispatch-message [user channel data]
  (let [{:keys [payload command]} (read-string data)]
    (when-let [listeners (get-in @*connected-users*
                                 [(:id user) :listeners command])]
      (doseq [l listeners] (l payload)))))

;; built-in actions

;; TODO: built-in handler to CLOSE THE CHAT ROOM?

(defn report-status [user _]
  (when-online user
   (let [info (get @*connected-users* (:id user))
         report-keys [:state :activity :room :last-connection]
         report (select-keys info report-keys)]
     (send-message user {:command :report-status :payload report}))))

(defn add-builtin-listeners [user]
  (add-listener user :status (partial report-status user)))

(defn remove-builtin-listeners [user]
  (remove-listeners user :status))

;; cleanup daemon

(def ^:private daemon-interval (* 5 1000)) ;; 5s
(def ^:private dead-timeout (* 10 1000)) ;; 10s

(defn- is-offline [u]
  (= (:state u) :offline))

(defn- cleanup-daemon []
  ;; remove long-offline users
  (loop [users (get-present-users is-offline) now-stamp (now)]
    (doseq [zombie-data (filter #(> (- now-stamp (:went-offline %)) dead-timeout)
                                users)]
      (println "removing:" (get-in zombie-data [:user :username]))
      (dispatch-message (:user zombie-data) nil {:command :disconnect})
      (swap! *connected-users* dissoc (get-in zombie-data [:user :id])))
    (Thread/sleep daemon-interval)
    (recur (get-present-users is-offline) (now))))

(defn start-cleanup-daemon []
  (future-call cleanup-daemon))

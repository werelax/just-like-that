(ns jlt.routes
  (:require [compojure.core :refer [defroutes GET POST PUT DELETE] :as compojure]
            [compojure.route :as route]
            [org.httpkit.server :as hk :refer [with-channel open? send!]]
            [ring.util.response :refer [response content-type redirect status resource-response]]
            [jlt.controllers.auth :as auth]
            [jlt.controllers.api :as api]
            [jlt.controllers.admin :as admin-controller]
            [jlt.controllers.assistant :as assistant]
            [jlt.controllers.user :as user]
            [jlt.services.assistant-matching :as assistant-matching]
            [jlt.services.chat :as chat]))


(def ^:dynamic *webdir* "html")

(defn- static-html [file]
  (-> (resource-response file {:root *webdir*})
    (content-type "text/html")))

(defn- render [fn & rest]
  (apply str (apply fn rest)))

;; ROUTES

(defroutes public-routes
  (GET "/" [] (static-html "home.html"))
  (GET "/home" [] (static-html "home.html"))
  (GET "/moreinfo" [] (static-html "more-info.html"))

  ;; *TEMPORARILY DISABLED*
  ;; (GET "/login" [] (static-html "login.html"))
  ;; (POST "/login" req (auth/create-session req))

  ;; (GET "/signup" [] (static-html "sign-up.html"))
  ;; (POST "/signup" req (auth/register-user req))

  ;; (GET "/a/signup" [] (static-html "signup-assitant.html"))
  ;; (POST "/a/signup" req (auth/register-assistant req))

  (GET "/login" [] (redirect "/preregister"))
  (GET "/signup" [] (redirect "/preregister"))
  (GET "/a/signup" [] (redirect "/preregister"))

  (GET "/preregister" [] (static-html "preregister.html")))

(defroutes private-routes
  (auth/with-auth

    ;; TODO: FIXME:
    ;; TODO: Some of this routes must be wrapped. In three ways:
    ;; TODO: * the dashboard routes should check if the user is on a chat room
    ;;         and just redirect if that is the case
    ;; TODO: * the chat routes should check if the user is already in *another*
    ;;         room and redirect.
    ;; TODO: * The logout routes should show a popup confirmation and close
    ;;         the chat room
    ;; TODO: * If I'm logged, go to my dashbaord!

    (GET "/me" req
      (assistant/if-assistant (auth/user req)
       (redirect "/dashboard")
       (user/dashboard req)))

    (GET "/dashboard" req
      (assistant/if-assistant (auth/user req)
       (assistant/dashboard req)
       (redirect "/me")))

    (GET "/presence" req
      (let [user (auth/user req)]
        (with-channel req channel
          (assistant/if-assistant user
           (assistant/with-channel channel user)
           (user/with-channel channel user)))))

    (GET "/query/:id" {{id :id} :params :as req}
      (if (chat/room-accesible? id (auth/user req))
        (assistant/if-assistant (auth/user req)
                                (redirect (str "/task/" id))
                                (user/chat req id))
        (redirect "/me")))

    (GET "/task/:id" {{id :id} :params :as req}
      (if (chat/room-accesible? id (auth/user req))
        (assistant/if-assistant (auth/user req)
                                (assistant/chat req id)
                                (redirect (str "/query/" id)))
        (redirect "/dashboard")))

    (GET "/logout" req (auth/destroy-session req))

    ;; TODO: XXX: WARNING: FIXME: DEBUG:

    (GET "/test/query" {{user :user} :session :as req}
      (assistant-matching/start-request-round user :flight)
      "ok...")

    ))

(defroutes fallback-routes
  (route/resources "/")
  (route/not-found "Oh, noes!"))

(def all-routes (compojure/routes public-routes private-routes fallback-routes))

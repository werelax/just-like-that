(ns jlt.core
  (:require [org.httpkit.server :as hk]
            [ring.middleware.reload :as reload]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.session.cookie :refer [cookie-store]]
            [ring.util.response :refer [response content-type]]
            ;; disabled forgery in site-defaults (not good for APIs)
            ;; [ring.middleware.anti-forgery :refer :all]
            [ring.middleware.session.cookie :refer [cookie-store]]
            ;; [ring.middleware.json :as json]
            [ring.middleware.edn :as edn]
            [compojure.core :refer [defroutes GET POST] :as compojure]
            [net.cgrand.enlive-html :as html :refer [deftemplate defsnippet]]
            [jlt.routes :as routes]
            [jlt.services.online-presence :as pressence])
  (:gen-class :main true))

(def ring-config (-> site-defaults
                   ;; default is in memory
                   (assoc-in [:session :store]
                             (cookie-store {:key "akandemorlakande"}))
                   ;; a pain for REST APIs
                   (assoc-in [:security :anti-forgery] false)))

(defn -main [& args]
  (pressence/start-cleanup-daemon)
  (let [handler (-> #'routes/all-routes
                  ;; comment/remove the next line in production
                  (reload/wrap-reload)
                  ;; (json/wrap-json-response)
                  ;; (json/wrap-json-params)
                  edn/wrap-edn-params
                  (wrap-defaults ring-config))
        stop-server (hk/run-server handler {:port 8080})]
    (println "Ok, good to go!")
    stop-server))

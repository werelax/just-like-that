(ns jlt.auth.strategy
  (:require [simpleauth.core :as auth]
            [jlt.entities :as entities]
            [korma.core :as db]))

(defn- get-id [user]
  (:id user (get user "id")))

(def strategy
  "default auth strategy. TODO: hash the password"
  (reify auth/AuthStrategy
    (serialize-user [_ user]
      (get-id user))
    (deserialize-user [_ user-id]
      (let [user (db/select entities/all-users
                            (db/where {:id user-id}))]
        (first user)))
    (check-credentials? [_ username password]
      (when-let [user (first (db/select entities/all-users
                                         (db/where {:username username})))]
         (when (= (:password user) password)
           user)))
  (login-route [_] "/login")))

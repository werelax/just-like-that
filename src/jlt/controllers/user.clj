(ns jlt.controllers.user
  (:require [net.cgrand.enlive-html :refer [deftemplate defsnippet] :as h]
            [org.httpkit.server :as hk]
            [jlt.models.user :refer [get-user-info ] :as user-model]
            [ring.util.response :refer [redirect]]
            [jlt.models.post :as post-model]
            [jlt.services.online-presence :as presence]
            [jlt.services.assistant-matching :as assistant-matching]))


(deftemplate user-dashboard "html/dashboard-user.html" []
  [:title] (h/content "Welcome"))

(deftemplate user-chat "html/dashboard.html" []
  [:title] (h/content "Welcome"))

(defn dashboard [req]
  (user-dashboard))

(defn chat [req id]
  (user-chat))

;; socket

(defn launch-query [user query-type]
  (presence/set-activity user :waiting)
  (assistant-matching/start-request-round user query-type))

(defn online [user channel]
  (presence/online user channel)
  (presence/add-listener user :query (partial launch-query user)))

(defn offline [user channel _]
  (presence/offline user channel _)
  (presence/remove-listeners user :query))

(defn with-channel [channel user]
  (hk/on-close channel (partial offline user channel))
  (hk/on-receive channel (partial presence/dispatch-message user channel))
  (online user channel))

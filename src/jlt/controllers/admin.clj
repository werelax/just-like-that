(ns jlt.controllers.admin
  (:require [net.cgrand.enlive-html :refer [deftemplate defsnippet] :as h]
            [jlt.models.user :refer [get-user-info ] :as user-model]
            [ring.util.response :refer [redirect]]
            [jlt.models.post :as post-model]))

;; (defsnippet post "html/admin-page-html.html" [:.post-history :li] [post]
;;   [:.header :h3] (h/content (:title post))
;;   [:.date :p] (h/content (:date post))
;;   [:.main :p] (h/content (:body post))
;;   [:.number] (h/content "0"))

;; (deftemplate admin-page "html/admin-page-html.html" [user]
;;   [:.user-data :.info :h3] (h/content (:username user))
;;   [:.user-data :.info :p] (h/content (:email user))
;;   [:.stats-info :.number] (h/content (str (count (:posts user))))
;;   [:.post-history] (h/content (map post (:posts user))))

;; (defn index [user]
;;   (admin-page (user-model/get-user-info (:id user))))

;; (defn create-post [user params]
;;   (post-model/create-with-user params user)
;;   (redirect "/admin"))

(ns jlt.controllers.assistant
  (:require [net.cgrand.enlive-html :refer [deftemplate defsnippet] :as h]
            [org.httpkit.server :as hk]
            [jlt.models.user :refer [get-user-info ] :as user-model]
            [ring.util.response :refer [redirect]]
            [jlt.services.online-presence :as presence]
            [jlt.models.post :as post-model]))

(deftemplate assistant-dashboard "html/dashboard-assistant.html" [user]
  [:title] (h/content "Welcome")
  [:.user-profile :img] (h/set-attr :src (:gravatar user))
  [:.user-data :h3] (h/content (:username user))
  [:.user-data :span] (h/content (:email user))
  )

(deftemplate assistant-chat "html/dashboard.html" []
  [:title] (h/content "Welcome"))

(defn is-assistant? [user]
  (= (:assistant user) 1))

(defmacro if-assistant [user ok fail]
  `(if (is-assistant? ~user) ~ok ~fail))

(defn dashboard [req]
  (assistant-dashboard (-> req :session :user)))

;; online presence code

(defn online [assistant channel]
  (presence/online assistant channel))

(defn offline [assistant channel _]
  (presence/offline assistant channel _))

(defn with-channel [channel user]
  (hk/on-close channel (partial offline user channel))
  (hk/on-receive channel (partial presence/dispatch-message user channel))
  (online user channel))

;; chat page

(defn chat [req id]
  (assistant-chat))

(ns jlt.controllers.auth
  (:require [simpleauth.core :as auth]
            [jlt.auth.strategy :refer [strategy]]
            [jlt.models.user :as user-model]
            [jlt.models.assistant :as assistant-model]
            [ring.util.response :refer [redirect]]))

(defn create-session [{params :params :as req}]

  (auth/create-session req strategy
                       {:username (get params :username)
                        :password (get params :password)}
                       (fn [req user] (redirect "/dashboard"))
                       (fn [_] (redirect "/login"))))

(defn- do-register [failroute okroute createfunc req params]
  (if-let [thing (createfunc params)]
    (auth/login {:req req :res (redirect okroute) :user thing})
    ;; TODO: put some errors on the session or something?
    (redirect failroute)))

(def do-register-user
  (partial do-register "/signup" "/me" user-model/create))
(def do-register-assistant
  (partial do-register "/a/signup" "/dashboard" assistant-model/create))

(defn- prepare-user-data [user-params]
  (-> user-params
    (dissoc :passwordconfirmation)
    (dissoc :r1)))

(defn check-password-confirm [func route {params :params :as req}]
  (if (= (:password params)
         (:passwordconfirmation params))
    (func req (prepare-user-data params))
    ;; TODO: put some errors on the session or something?
    (redirect route)))

(def register-user
  (partial check-password-confirm do-register-user "/signup"))

(def register-assistant
  (partial check-password-confirm do-register-assistant "/a/signup"))

(defn destroy-session [req]
  (auth/destroy-session req (redirect "/login")))

(defn with-auth [& routes]
  (apply auth/with-auth (list* strategy routes)))

(defn user [req]
  (-> req :session :user))

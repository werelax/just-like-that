(ns jlt.entities
  (:require [korma.db :as db]
            [korma.core :refer :all]
            [korma.config :as config]))

;; utils

(defn id [response]
  (first (vals response)))

;; connection (maybe this is not the best place...)

(db/defdb db (db/h2 {:subname "db/dev"}))
;; (config/set-delimiters "")

(declare users assistants assistants-data posts jobs results)

(defentity all-users
  (pk :id)
  (table :users)
  (database db)
  (has-many posts))

(defentity users
  (table (subselect all-users (where (= :assistant nil)))
         :assistants))

(defentity assistants
  (table (subselect all-users (where (not= :assistant nil)))
         :assistants))

(defentity assistant-data
  (pk :id)
  (table :assistantsdata)
  (database db)
  (belongs-to assistants))

(defentity posts
  (pk :id)
  (table :posts)
  (database db)
  (belongs-to users)
  (belongs-to assistants))

(defentity jobs
  (pk :id)
  (table :jobs)
  (database db)
  (belongs-to users)
  (belongs-to assistants)
  (has-many results))

(defentity results
  (pk :id)
  (table :results)
  (database db)
  (belongs-to jobs))

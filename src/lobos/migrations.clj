(ns lobos.migrations
  (:refer-clojure :exclude [alter drop bigint boolean char double float time])
  (:use (lobos [migration :only [defmigration]] core schema config)))

(defmigration create-users-table
  (up [] (create
          (table :users
                 (integer :id :auto-inc :primary-key)
                 (varchar :email 150)
                 (varchar :username 150)
                 (varchar :gravatar 250)
                 (integer :assistant)
                 (varchar :password 30)
                 (varchar :password_hash 100)
                 (varchar :salt 30))))
  (down [] (drop (table :users))))

(defmigration create-assitant-data-table
  (up [] (create
          (table :assistantsdata
                 (integer :id :auto-inc :primary-key)
                 (integer :assistants_id [:refer :users :id :on-delete :set-null])
                 ;; TODO: a REAL variable size string field?
                 (varchar :blob 5000))))
  (down [] (drop (table :assistants))))

(defmigration create-posts-table
  (up [] (create
          (table :posts
                 (integer :id :auto-inc :primary-key)
                 (integer :users_id [:refer :users :id :on-delete :set-null])
                 (integer :assistants_id [:refer :users :id :on-delete :set-null])
                 (varchar :body 1000)
                 (integer :created_at))))
  (down [] (drop (table :posts))))

(defmigration create-jobs-table
  (up [] (create
          (table :jobs
                 (integer :id :auto-inc :primary-key)
                 (integer :users_id [:refer :users :id :on-delete :set-null])
                 (integer :assistants_id [:refer :users :id :on-delete :set-null])
                 (integer :status)
                 (integer :created_at)
                 )))
  (down [] (drop (table :jobs))))

(defmigration create-results-table
  (up [] (create
          (table :results
                 (integer :id :auto-inc :primary-key)
                 (integer :jobs_id [:refer :jobs :id :on-delete :set-null])
                 ;; TODO: a REAL variable size string field?
                 (varchar :data 1000)
                 (integer :created_at))))
  (down [] (drop (table :results))))

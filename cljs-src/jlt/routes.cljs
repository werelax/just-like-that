(ns jlt.routes
  (:require [secretary.core :as secretary :refer-macros [defroute]]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [jlt.pages.assistant-chat :as assistant-chat]
            [jlt.pages.user-chat :as user-chat]
            [jlt.pages.assistant-dashboard :as assistant-dashboard]
            [jlt.pages.user-dashboard :as user-dashboard]
            [jlt.pages.preregister-bot :as preregister-bot]))

(defroute "/dashboard" []
  (assistant-dashboard/start))

(defroute "/me" []
  (user-dashboard/start))

(defroute "/query/:id" [id]
  (user-chat/start id))

(defroute "/task/:id" [id]
  (assistant-chat/start id))

(defroute "/preregister" []
  (preregister-bot/start))

(set! (.-onload js/window)
      #(let [route (.. js/window -location -pathname)]
         (secretary/dispatch! route)))

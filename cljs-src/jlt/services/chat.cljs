(ns jlt.services.chat
  (:require [cljs.reader :as edn]))

(defn- chat-handler [user room socket]
  {:user user :room room :socket socket})

(defn connect-to-room [user room]
  (let [url (str "ws://" (-> js/window .-location .-host) "/chat?=" room)
        socket (js/WebSocket. url)]
    (chat-handler user room socket)))

(defn send-message [handler data]
  (let [msg (pr-str data)
        socket (:socket handler)]
    (.send socket msg)))

(defn on-message [handler func]
  (let [socket (:socket handler)]
    (set! (.-onmessage socket)
          #(let [data (.-data %)
                 msg (edn/read-string data)]
             (func msg)))))

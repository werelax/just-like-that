(ns jlt.services.online-presence
  (:require [cljs.reader :as edn]))

(def *socket* (atom nil))
(def *socket-ready* (atom false))
(def *listeners* (atom {}))

(defn dispatch-message [ev]
  (js/console.log "<--" (.-data ev))
  (let [{:keys [command payload]} (edn/read-string (.-data ev))
        listeners (get @*listeners* command)]
    (doseq [l listeners] (l payload))))

(defn send-command [command payload]
  (.send @*socket* (pr-str {:command command :payload payload})))

(defn add-listener [command func]
  (swap! *listeners* update-in [command] #(conj % func)))

(defn remove-listeners [command]
  (swap! *listeners* dissoc command))

(defn add-once-listener [command func]
  (add-listener command #(do (remove-listeners command)
                             (func %))))

(defn when-ready [func]
  (if (not @*socket-ready*)
    (let [prevfn (.-onopen @*socket*)]
      (set! (.-onopen @*socket*) (fn [] (prevfn) (func))))
    (func)))

(defn connect []
  (let [host (-> js/window .-location .-host)
        url (str "ws://" host "/presence")
        socket (js/WebSocket. url)]
    (set! (.-onopen socket) #(swap! *socket-ready* (constantly true)))
    (set! (.-onmessage socket) dispatch-message)
    (swap! *socket* (constantly socket))))

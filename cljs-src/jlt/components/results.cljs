(ns jlt.components.results
  (:require [kioo.om :refer [content] :as k]
            [kioo.core :refer [handle-wrapper]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :as async :refer [chan put!]])
  (:require-macros [kioo.om :refer [defsnippet deftemplate]]))

;; some utils

(defn prev-default [func]
  (fn [e]
    (.preventDefault e)
    (func)))

(defn input-state [owner state keyword]
  (k/do-> (k/set-attr :value (keyword state))
          (k/listen :onChange #(om/set-state! owner keyword
                                              (-> % .-currentTarget .-value)))))

(defn clear-state [owner state]
  (let [clean-state (apply hash-map
                           (interleave (keys state) (repeat "")))]
    (om/set-state! owner clean-state)))

(defn report-result [channel owner state]
  (put! channel {:command :result-message :payload state})
  (clear-state owner state))

;; UI code

(defsnippet result-row "html/dashboard.html" [:.results.right :li]
  [{:keys [title description price link]}]
  {[:.title] (k/content title)
   [:.description] (k/content description)
   [:.link] (k/do->
             (k/content link)
             (k/set-attr :href link))
   [:.go] (k/set-attr :href link)
   [:.price] (k/content (str price "€"))})


(defsnippet footer "html/dashboard.html" [:.results-footer]
  [owner {channel :channel} {:keys [title description link price] :as state}]
  {[k/root] (k/set-attr :style {:display nil})
   [:.js-title] (input-state owner state :title)
   [:.js-description] (input-state owner state :description)
   [:.js-link] (input-state owner state :link)
   [:.js-price] (input-state owner state :price)
   [:.js-send] (k/listen :onClick (prev-default
                                   #(report-result channel owner state)))
   [:.js-clear] (k/listen :onClick (prev-default
                                    #(clear-state owner state)))})

(defn footer-form [data owner]
  (reify
    om/IInitState (init-state [_] {:title "" :description "" :link "" :price ""})
    om/IRenderState (render-state [_ state] (footer owner data state))))

(defsnippet user-results-window "html/dashboard.html" [:.results.right]
  [{results :results :as data}]
  {[:.js-name] (k/content (get-in data [:assistant :username] ""))
   [:.results-list] (k/content (map result-row results))
   [:.results-footer] (k/substitute nil)})

(defsnippet assistant-results-window "html/dashboard.html" [:.results.right]
  [{results :results :as data}]
  {[:.js-name] (k/content (get-in data [:assistant :username] ""))
   [:.results-list] (k/content (map result-row results))
   [:.results-footer] (k/substitute (om/build footer-form data))})

(defn user-root [data]
  (om/component (user-results-window data)))

(defn assistant-root [data]
  (om/component (assistant-results-window data)))

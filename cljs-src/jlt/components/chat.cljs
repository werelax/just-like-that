(ns jlt.components.chat
  (:require [kioo.om :refer [content] :as k]
            [kioo.core :refer [handle-wrapper]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [jlt.services.chat :as chat]
            [cljs.core.async :as async :refer [chan put!]])
  (:require-macros [kioo.om :refer [defsnippet deftemplate]]))

;; public channel

(def c (chan 1))

;; TODO: I know, having only ONE channel means I can have AT MOST one instance
;;       of this component... in a better world, the channel come

;; (defn on-keyup [e]
;;   (let [key-code (.-keyCode e)
;;         handler (:handler @state)
;;         body (:input @state)]
;;     (when (= key-code 13)
;;       (chat/send-message handler {:message body})
;;       (append-message :right body)
;;       (swap! state assoc :input "")))
;;  )

(defsnippet left-message "html/dashboard.html" [:.chat-row.img-left]
  [data message]
  {[k/root] (k/set-attr :style {:display nil}) ;; remove the display:none
   [:.chat-img :img] (k/set-attr :src (get-in data [(:from message) :gravatar]))
   [:.name] (k/content (str (get-in data [(:from message) :username]) ":"))
   [:.js-text] (k/content (:text message))})

(defsnippet right-message "html/dashboard.html" [:.chat-row.img-right]
  [data message]
  {[k/root] (k/set-attr :style {:display nil}) ;; remove the display:none
   [:.chat-img :img] (k/set-attr :src (get-in data [(:from message) :gravatar]))
   [:.name] (k/content (str (get-in data [(:from message) :username]) ":"))
   [:.js-text] (k/content (:text message))})

(defn on-keydown [owner text channel e]
  (let [key-code (.-keyCode e)]
    (when (and (= key-code 13) (not= text ""))
      (put! channel {:command :chat-message :payload text})
      (om/set-state! owner :text ""))))

(defsnippet footer "html/dashboard.html" [:.chat-footer]
  [owner {channel :channel} {text :text}]
  {[:input] (k/do->
             (k/set-attr :value text)
             (k/listen :onKeyDown (partial on-keydown owner text channel))
             (k/listen :onChange #(om/set-state! owner :text
                                                 (-> % .-currentTarget .-value))))})

(defn footer-component [data owner]
  (reify
    om/IInitState (init-state [_] {:text ""})
    om/IRenderState (render-state [_ state]
                      (footer owner data state))))

(defn is-from-me? [data msg]
  (= (:from msg) (:me data)))

(defsnippet chat-window "html/dashboard.html" [:.chat.left]
  [data]
  {
   [:.js-name] (k/content (get-in data [:assistant :username] ""))
   [:.chat-content] (k/do->
                     (k/set-attr :ref "scroller")
                     (k/content (map #(if (is-from-me? data %)
                                        (right-message data %)
                                        (left-message data %))
                                     (:messages data))))
   [:.chat-footer] (k/substitute (om/build footer-component data))
   })

(defn scroll-to-bottom [node]
  (set! (.-scrollTop node) (.-scrollHeight node)))

;; public

(defn root [data owner]
  (reify
    om/IRender (render [_] (chat-window data))
    om/IDidUpdate (did-update [_ pp ps]
                    (scroll-to-bottom (om/get-node owner "scroller")))))

(ns jlt.pages.preregister-bot
  (:require [om.core :as om :include-macros true]
            [kioo.om :as k]
            [cljs.core.async :as async :refer [alts! chan]]
            [jlt.services.online-presence :as presence]
            [jlt.components.chat :as chat-component]
            [jlt.components.results :as results-component])
  (:require-macros [kioo.om :refer [defsnippet deftemplate]]
                   [cljs.core.async.macros :refer [go go-loop alt!]]))

;; some config

(def bot-data
  {:avatar "/assets/images/logo.png"})

(def user-data
  {:avatar "/assets/images/default-avatar.png"})

(def state (atom {:messages [{:from :bot :text "Hi there, nice to meet you! It's JustLikeBot here."}]}))

;; bot logic!

(declare yes-no go-to-node check-email end-of-conversation)
(def current-node (atom :message1))

(def messages
  {:message1 {:text ["Do you want to pre-register so you are sure to be the first to get an invite for our app?"]
              :pred #(yes-no :message2 :message3 %)}
   :message2 {:text ["Great news! Tell me your email address and I will make sure to send you and invite!"]
              :pred #(check-email :message4 %)}
   :message3 {:text ["It's a pity! If you change your mind don't hesitate to get back to me."]
              :pred #(go-to-node :message5)}

   :message4 {:text ["Great! We will be in touch shortly ;)"]
              :pred #(end-of-conversation)}
   :message5 {:text ["Hey! Did you change your mind?"]
              :pred #(yes-no :message2 :message3 %)}})

(defn delay [func d]
  (js/setTimeout func d))

(defn bot-says [text]
  (js/setTimeout
   (fn []
     (-> (js/Audio. "/assets/snap.wav") .play)
     (swap! state update-in [:messages] #(conj % {:from :bot :text text})))
   700))

(defn user-says [text]
  (swap! state update-in [:messages] #(conj % {:from :user :text text})))

(defn try-again []
  (bot-says "Sorry, I don't understand that! Try with a simple yes or no :)"))

(defn try-email-again []
  (bot-says "Hmm... Sorry, I'm not sure I understood. Can you write down your email again?"))

(defn process-yn-input [text]
  (cond
    (re-matches #"(?i).*yes.*" text) :yes
    (re-matches #"(?i).*no.*" text) :no
    :else :else))

(defn valid-email? [text]
  (re-matches
   #"(?i)[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
   text))

(defn end-of-conversation [text]
  (bot-says "My job is done here! Spread the word and have a nice day."))

(defn go-to-node [key]
  (swap! current-node (constantly key))
  (doseq [t (get-in messages [key :text])]
    (bot-says t)))

(defn yes-no [if-yes if-no text]
  (case (process-yn-input text)
    :yes (go-to-node if-yes)
    :no (go-to-node if-no)
    :else (try-again)))

(defn check-email [if-correct email]
  (if (valid-email? email)
    (go-to-node if-correct)
    (try-email-again)))

(defn process-user-response [text]
  (user-says text)
  (let [pred (get-in messages [@current-node :pred])]
    (pred text)))

;; message list

(defsnippet left-message "html/preregister.html" [:.chat-row.img-left]
  [{text :text}]
  {[k/root] (k/set-attr :style {:display nil}) ; make it visible!
   [:.chat-img :img] (k/set-attr :src (:avatar bot-data))
   [:.name] (k/content "JustLikeBot:")
   [:.js-text] (k/content text)})

(defsnippet right-message "html/preregister.html" [:.chat-row.img-right]
  [{text :text}]
  {[k/root] (k/set-attr :style {:display nil}) ; make it visible!
   [:.chat-img :img] (k/set-attr :src (:avatar user-data))
   [:.name] (k/content "You:")
   [:.js-text] (k/content text)})

(defn message [{from :from :as msg-data}]
  (case from
    :bot (left-message msg-data)
    :user (right-message msg-data)))

(defsnippet chat-messages-snippet "html/preregister.html" [:.chat-content]
  [messages]
  {[k/root] (k/do->
             (k/content (map #(message %) messages))
             (k/set-attr :ref "scroller"))})

(defn scroll-to-bottom [node]
  (set! (.-scrollTop node) (.-scrollHeight node)))

(defn chat-messages [messages owner]
  (reify
    om/IRender (render [_] (chat-messages-snippet messages))
    om/IDidUpdate (did-update [_ pp ps]
                    (scroll-to-bottom (om/get-node owner "scroller")))))

;; footer (form)

(defn submit-if-enter [owner text e]
  (let [key-code (.-keyCode e)]
    (when (and (= key-code 13) (not= text ""))
      (process-user-response text)
      (om/set-state! owner :text ""))))

(defsnippet footer-snippet "html/preregister.html" [:.chat-footer]
  [owner data {text :text}]
  {[:input] (k/do->
             (k/set-attr :value text)
             (k/listen :onChange #(om/set-state! owner :text
                                                 (-> % .-currentTarget .-value)))
             (k/listen :onKeyDown (partial submit-if-enter owner text)))})

(defn chat-footer [data owner]
  (reify
    om/IInitState (init-state [_] {:text ""})
    om/IRenderState (render-state [_ state]
                      (footer-snippet owner data state))))

;; page layout + init

(deftemplate layout "html/preregister.html"
  [{messages :messages :as data}]
  {[:.chat-content] (k/substitute (om/build chat-messages messages))
   [:.chat-footer] (k/substitute (om/build chat-footer data))})

(defn root-component [data]
  (om/component (layout data)))

(defn start []
  (om/root root-component state {:target (.-body js/document)})
  (go-to-node :message1))

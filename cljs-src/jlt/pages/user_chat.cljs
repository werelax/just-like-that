(ns jlt.pages.user-chat
  (:require [kioo.om :refer [content] :as k]
            [om.core :as om :include-macros true]
            [cljs.core.async :as async :refer [alts! chan]]
            [jlt.services.online-presence :as presence]
            [jlt.components.chat :as chat-component]
            [jlt.components.results :as results-component])
  (:require-macros [kioo.om :refer [defsnippet deftemplate]]
                   [cljs.core.async.macros :refer [go go-loop alt!]]))

(def state (atom {:me :user
                  :him :assistant
                  :channel (chan 1)
                  :messages []
                  :results [{:title "asdf" :description "jajaja" :price 12.89 :link "http:///www.google.com"}]
                  :session-info :pending}))

(declare finish-task)

;; TODO: time the session?
;; TODO: REFACTOR!!

(deftemplate layout "html/dashboard.html"
  [data]
  {[:.chat.left] (k/substitute (om/build chat-component/root data))
   [:.results.right] (k/substitute (om/build results-component/user-root data))
   [:.js-name] (k/content (get-in data [:assistant :username] ""))
   [:.button-row :.button] (k/listen :onClick finish-task)})

(defn root-component [data]
  (om/component (layout data)))

;; async.flux

(defn append-and-send-message [message]
  (let [msg-payload {:from (:me @state) :text message}]
    (presence/send-command :send-message msg-payload)
    (swap! state update-in [:messages] #(conj % msg-payload))))

(defn append-and-send-result [result]
  (presence/send-command :send-result result)
  (swap! state update-in [:results] #(conj % result)))

(go-loop [{:keys [command payload] :as cmd} (<! (:channel @state))]
  (case command
    :log (js/console.log "LOG:" payload)
    :chat-message (append-and-send-message payload)
    :result-message (append-and-send-result payload)
    :else (js/console.log "unknown command:" cmd))
  (recur (<! (:channel @state))))

;; chat logic

(defn on-status-report [data]
  "didn't need this one yet")

(defn on-chat-status [data]
  (swap! state (constantly (merge @state data {:session-info :ready}))))

(defn on-new-message [data]
  (swap! state update-in [:messages] #(conj % data)))

(defn on-new-result [data]
  (swap! state update-in [:results] #(conj % data)))

(defn finish-task []
  (presence/send-command :finish-task nil))

(defn on-room-closed [room]
  (set! (-> js/window .-location .-href) "/me"))

;; initialization

(defn start [id]
  (presence/connect)
  (presence/add-listener :report-status on-status-report)
  (presence/add-listener :report-chat-status on-chat-status)
  (presence/add-listener :new-message on-new-message)
  (presence/add-listener :new-result on-new-result)
  (presence/add-listener :room-closed on-room-closed)
  (presence/when-ready
   #(do (presence/send-command :status nil)
        (presence/send-command :chat-status nil)))
  (om/root root-component state {:target (.-body js/document)}))

(ns jlt.pages.user-dashboard
  (:require [jlt.services.online-presence :as presence]
            [jayq.core :refer [$] :as jq]))

(defn loading-state []
  (-> ($ :.top-banner)
    (jq/add-class :loading)))

(defn normal-state []
  (-> ($ :.top-banner)
    (jq/remove-class :loading)
    (jq/remove-class :error)))

(defn error-state []
  (normal-state)
  (-> ($ :.top-banner)
    (jq/add-class :error))
  (js/setTimeout normal-state 5000))

(defn send-query-request [e]
  (.preventDefault e)
  (loading-state)
  (presence/send-command :query :flight))

(defn bind-events []
  (let [buttons ($ :.task)]
    (jq/on buttons :click send-query-request)))

(defn go-to-chat [room]
  (let [url (str "/query/" room)]
    (-> js/window .-location .-href (set! url))))

(defn start []
  (normal-state)
  (presence/connect)
  (presence/add-listener :report-status #(js/console.log "status:" (pr-str %)))
  (presence/add-listener :query-aborted error-state)
  (presence/add-listener :chat-ready go-to-chat)
  (presence/when-ready
   #(presence/send-command :status nil))
  (bind-events))

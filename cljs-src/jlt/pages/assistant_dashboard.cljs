(ns jlt.pages.assistant-dashboard
  (:require [jlt.services.online-presence :as presence]
            [jayq.core :refer [$] :as jq]))

(def *task-timeout* (* 10 1000))

(defn remove-listeners [notification]
  (jq/off notification :click))

(defn cleanup-popup [notification]
  (remove-listeners notification)
  (jq/remove-class notification :open-notification))

(defn on-accept []
  (presence/send-command :answer-to-request :ok))

(defn on-reject []
  (presence/send-command :answer-to-request :reject))

(defn go-to-chat [room]
  (let [url (str "/task/" room)]
    (-> js/window .-location .-href (set! url))))

(defn bind-listeners [notification]
  (let [cleanup (partial cleanup-popup notification)]
    (jq/on notification :click :.ok (comp cleanup on-accept))
    (jq/on notification :click :.ko (comp cleanup on-reject))))

(def start-timer (let [timer-id (atom nil)]
                   (fn [n]
                     (js/clearTimeout @timer-id)
                     (swap! timer-id
                            (partial js/setTimeout (partial cleanup-popup n)
                                     *task-timeout*)))))

(defn show-notification [type]
  (let [notification ($ :.assistant-dashboard)]
    (-> notification
      (jq/find ".notification h2")
      (jq/html (str "You have a " type " mission!")))
    (bind-listeners notification)
    (start-timer notification)
    (jq/add-class notification :open-notification)))

(defn start []
  (presence/connect)
  (presence/add-listener :report-status #(js/console.log "status:" (pr-str %)))
  (presence/add-listener :task-request show-notification)
  (presence/add-listener :chat-ready go-to-chat)
  (presence/when-ready
   #(presence/send-command :status nil)))
